# rtbf - Real-time beamforming for ultrasound using GPUs

## Announcements

The first "complete" release of `rtbf` (version 1.0.0) is live!

See the [changelog](CHANGELOG.md) for the latest changes and updates to the repository.

## Motivation

Ultrasound scanners produce large amounts of raw data that are then processed to reconstruct images. Previously, application-specific hardware was used to process and downsample the data into a more manageable form (e.g., apply time delays and sum channels). With recent technological advances, especially in graphics processing unit (GPU)-based computing, it is now feasible to obtain ultrasound data from an earlier point in the processing pipeline and perform this beamforming entirely in software at real-time rates. Real-time software beamforming allows the direct implementation and study of experimental algorithms designed to improve ultrasound imaging.

`rtbf` is a GPU-based implementation of ultrasound beamforming, with the goal of implementing custom algorithms for real-time imaging.

![images/phantom2.gif](images/phantom2.gif)

Examples of real-time imaging using a custom [speckle-reducing neural network beamformer](https://gitlab.com/dongwoon.hyun/nn_bmode).

## Installation Instructions

Detailed step-by-step installation instructions for Linux and Windows are now provided in [INSTALL.md](INSTALL.md).

## Code structure

This repository utilizes the CUDA C/C++ API created by NVIDIA and consists of three main components: `gpuBF`, `vsxBF`, and `annBF`.

### gpuBF

`gpuBF` is a pure CUDA C/C++ library of beamforming classes. The code is structured around two main classes: [`DataArray`](gpuBF/DataArray.cuh) and [`DataProcessor`](gpuBF/DataProcessor.cuh).

[`DataArray`](gpuBF/DataArray.cuh) is a class template that encapsulates a pointer to GPU memory as well as to the descriptors of this memory (e.g., GPU index, pitch). A [`DataArray`](gpuBF/DataArray.cuh) also provides convenience functions to facilitate data transfer between the host and GPU device (e.g., `copyToGPU`, `copyFromGPUAsync`). [`DataArray`](gpuBF/DataArray.cuh)s are represented as 3-dimensional arrays with dimensions `(nx, ny, nc)`, where `nx` and `ny` are two spatial dimensions and `nc` is the channel dimension. The first dimension is pitched for more efficient memory access and compatibility with CUDA texture and surface objects.

[`DataProcessor`](gpuBF/DataProcessor.cuh) is an abstract base class that accepts a pointer to a [`DataArray`](gpuBF/DataArray.cuh) as input, applies some user-defined processing functions, and produces a [`DataArray`](gpuBF/DataArray.cuh) as output. [`DataProcessor`](gpuBF/DataProcessor.cuh)s can be tagged as belonging to specific CUDA streams to enable concurrent execution and/or multi-GPU processing. Some key examples of [`DataProcessor`](gpuBF/DataProcessor.cuh)s include:

* [`Focus`](gpuBF/Focus.cuh)
  Applies specified time delays to channel data. Assumes one transmit beam to one receive beam.
* [`FocusSynap`](gpuBF/FocusSynap.cuh)
  Applies specified time delays to channel data with retrospective aperture synthesis (e.g., plane wave imaging). Can be used to coherently sum multiple transmit firings or receive elements in a focused manner (i.e. _synthesize_ a transmit or receive aperture).
* [`ChannelSum`](gpuBF/ChannelSum.cuh)
  Sums the channels into `N` equal subapertures. By default, `N = 1;`, i.e., all channels are summed into a single signal.
* [`Bmode`](gpuBF/Bmode.cuh)
  Applies envelope detection, and optionally, logarithmic compression. If more than one channel is provided (`nc > 1`), the respective images are summed incoherently (e.g., spatial compounding).
* [`NeuralNetwork`](annBF/utils/NeuralNetwork.cuh)
  Uses NVIDIA's TensorRT framework to perform real-time inference of an artificial neural network. [`NeuralNetwork`](annBF/utils/NeuralNetwork.cuh) converts a frozen TensorFlow graph with a single input and a single output node. See `annBF` below for more details.
* [`VSXDataFormatter`](vsxBF/utils/VSXDataFormatter.cuh)
  Provides a simplified interface to process raw receive buffer data from a Verasonics research system. The [`VSXDataFormatter`](vsxBF/utils/VSXDataFormatter.cuh) unwraps, resamples, and reshapes the data for easy integration with the rest of `gpuBF`. See `vsxBF` below for more details.
* In development: [`HilbertTransform`](gpuBF/HilbertTransform.cuh), [`GenericFilter`](gpuBF/GenericFilter.cuh), [`EnsembleFilter`](gpuBF/EnsembleFilter.cuh), [`PowerEstimator`](gpuBF/PowerEstimator.cuh), [`VelocityEstimator`](gpuBF/VelocityEstimator.cuh).

### vsxBF

`vsxBF` provides an interface between `gpuBF` and a Verasonics research ultrasound system. Several utilities are included, such as the `VSXDataFormatter`, which applies Verasonics-specific formatting to the raw input data stream (e.g., direct quadrature sampling), as well as utilities to copy GPU data to MATLAB arrays. Several example scripts are provided as well.

### annBF

`annBF` consists of python implementations of artificial neural networks that are saved into a format that can be loaded into `gpuBF` via NVIDIA's TensorRT API. TensorRT is currently restricted to a small list of supported operations, but as this list expands, it will become possible to implement custom beamforming algorithms as neural networks (whether they include machine learning or not), with optimization performed via TensorRT rather than manual CUDA kernel tuning.

## Example Usage

An example of synthetic transmit aperture beamforming is shown below. The following code snippets are adapted from [vsxBF/L12-3v/bmode/L12_3v_bmode_synap.cu](vsxBF/L12-3v/bmode/L12_3v_bmode_synap.cu), which shows an example of a MATLAB MEX function interface to `rtbf`.

### Declaration

To use the code, we first declare the desired processing objects as static objects with file scope:

```c++
static rtbf::VSXDataFormatter<short2> D;    // Format raw data
static rtbf::FocusSynAp<short2, float2> F;  // Focus raw data
static rtbf::ChannelSum<float2, float2> S;  // Sum focused data
static rtbf::Bmode<float2, float> B;        // Detect envelope
```

The `static` modifier declares that we want these four objects to have a lifetime that persists over multiple calls to the MATLAB mex function.

Additionally, we want the formatted data to have datatype `short2` (i.e., a vector of two `int16`s corresponding to the real and imaginary components). The focusing object will take a `DataArray<short2>` as input and output a `DataArray<float2>` result. The channel sum will take in a `DataArray<float2>` of channel data and output a `DataArray<float2>`. Finally, the B-mode object will convert a `DataArray<float2>` into the final output envelope image of type `DataArray<float>`.

### Initialization

Prior to real-time execution, each object must be initialized based on the imaging configuration. Initialization is a time-consuming step because of memory allocations and one-time computations needed to set up the computational graph.

```c++
D.initialize(nsamps, nxmits, nelems, nchans);           // Data formatter
F.initialize(&D, nrows, ncols, outspwl, delTx, delRx);  // Focus synthetic aperture
S.initialize(&F, 1);                                    // Channel sum
B.initialize(&S);                                       // B-mode image
```

This initialization routine only needs to be called once for a given imaging configuration. Observe that subsequent objects are initialized by passing in pointers to the previous object (i.e., `F.initialize(&D, ...`), allowing the relevant parameters and GPU data pointers to be passed along automatically. Only the default arguments are shown below, but more complex configurations are possible (e.g., GPU selection, streamed execution, class-specific options). Check the `.cuh` header file for each  `DataFormatter` subclass for further details.

### Execution

Once the computational graph is initialized, each component can be executed sequentially.

```c++
D.formatRawVSXData(h_raw, pitchInSamps);  // Format raw data pointed to by h_raw
F.focus();                                // Focus the raw data
S.sumChannels();                          // Sum the focused channels
B.getEnvelopeLogCompress();               // Output a log-compressed envelope image
```

This code block executes the full computational graph: raw data in CPU RAM is copied to the GPU, formatted, focused, summed, and output as a log-compressed B-mode image. On most GPUs and imaging configurations, this execution completes in under 30 ms (and often much less), giving >30 frames per second for real-time imaging.
