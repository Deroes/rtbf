/**
 @file vsxBF/utils/VSXDataFormatter.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-27

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef VSXDATAFORMATTER_CUH_
#define VSXDATAFORMATTER_CUH_

#include "DataProcessor.cuh"

namespace rtbf {

/// @brief Enum class to describe VSX sampling mode
enum class VSXSampleMode { BS50BW = 1, BS100BW = 2, NS200BW = 4, HILBERT = 0 };

/** @brief Enum class to describe data ordering

When performing Doppler acquisitions, five dimensions of data are acquired:
- Samples (i.e., fast time)
- Pulses (e.g., pulse-inversion harmonic imaging, averaging to improve SNR)
- Transmits (e.g., focused transmits or plane waves)
- Doppler Frames (i.e., the ensemble dimension)
- Channels

The sample dimension is always the fastest changing and the channel dimension is
always the slowest changing. Pulses, transmits, and Doppler frames can be
permuted arbitrarily depending on the application. DopplerFormat enumerates the
various permutations.

The following formats are supported:
  1. PXF = [Pulses, Transmits, Frames]  e.g., "ultrafast" Doppler
  2. PFX = [Pulses, Frames, Transmits]  e.g., traditional Doppler
  3. XPF = [Transmits, Pulses, Frames]
  4. FPX = [Frames, Pulses, Transmits]
  5. XFP = [Transmits, Frames, Pulses]
  6. FXP = [Frames, Transmits, Pulses]
*/
enum class VSXDataOrder { PXF, PFX, XPF, FPX, XFP, FXP };

/** @brief Class to format Verasonics channel data

This class accepts a host pointer to the raw receive channel buffer, copies it
over to a device pointer, and applies preprocessing.

There are three types of preprocessing that can be applied:
  1. Direct baseband sampling via Verasonics's sampleMode parameter
  2. Coherent summing of pulses (e.g., pulse-inversion harmonic imaging)
  3. Channel mapping unwrapping

The Verasonics scanner provides the option to directly sample the IQ baseband
data, via setting the "Receive.sampleMode" parameter. (See the Verasonics
Sequence Programming Manual for more details.) These modes can be selected by
setting the VSXSampleMode. Supported values are: NS200BW, BS100BW, and BS50BW.
Alternatively, the user can choose to treat the signals as the real component of
the RF signal and obtain the modulated IQ signal via the Hilbert transform using
the value HILBERT.

The class also provides the option to sum (i.e. coherently compound) multiple
input pulses. This is useful for cases like pulse-inversion harmonic imaging,
where the positive and negative phase pulses can be summed immediately to reduce
the data size.

Often, the channel mapping is some permutation of the element positions. When an
optional host pointer to the channel mapping is specified, the channels are
unwrapped automatically. Furthermore, the number of channels per acquisition may
differ from the total number of channels to be stored for processing (e.g.,
walked aperture, multiplexed transducers). In these cases, the user should
provide a mapping of acquired channels to actual channels on a per-transmit
basis via the h_channelMapping argument.

These operations are all fused into a single kernel to minimize the overhead
associated with launching a CUDA kernel.
*/

template <typename T>
class VSXDataFormatter : public DataProcessor<T> {
 protected:
  // VSXDataFormatter members
  int nsamps;   ///< Number of samples per transmit/receive event
  int nxmits;   ///< Number of transmit/receive events to store
  int nchans;   ///< Number of total channels (i.e., number of elements)
  int nacqch;   ///< Number of channels acquired at a time
  int npulses;  ///< Number of pulses to sum (e.g., pulse-inversion imaging)
  int nframes;  ///< Number of frames to process (e.g., Doppler ensemble length)
  VSXSampleMode mode;      ///< Data sample mode (e.g., NS200BW)
  DataArray<short2> in;    ///< Input unformatted data (int16 pairs)
  DataArray<int> chanmap;  ///< Channel mapping (0-indexed, nacqch-by-nxmits)
  DataArray<float2> tmp;   ///< Temporary array of data type float2 for FFT
  int ppitch;
  int xpitch;
  int fpitch;
  cufftHandle fftplan;  ///< CUFFT plan object

  // Internal functions
  void getPitches(int *pstride, int *xstride, int *fstride, VSXDataOrder order);
  void unwrapData(short2 *idata);
  void filterData();
  void downsampleData(T *odata);

 public:
  VSXDataFormatter();
  virtual ~VSXDataFormatter();

  /// @brief Free all dynamically allocated memory.
  void reset();

  /// @brief Initialization function
  void initialize(int numSamples, int numTransmits, int numChannels,
                  int numAcqChannels, int numFrames = 1,
                  VSXSampleMode sampleMode = VSXSampleMode::NS200BW,
                  int numPulsesToSum = 1, int *h_channelMapping = nullptr,
                  int deviceID = 0, cudaStream_t cudaStream = 0,
                  int verbosity = 1);

  /// @brief Function to execute formatting.
  void formatRawVSXData(short *h_raw, int pitchInSamps);
};

namespace VSXDataFormatterKernels {
// CUDA kernels
// Unwrap the IQ into our fully-sampled temporary array with inserted zeros
template <int spc>
__global__ void unwrapIQ(int nsamps, int npulses, int nxmits, int nacqch,
                         int nchans, int ppitch, int xpitch, short2 *idata,
                         int ipitch, float2 *tdata, int tpitch, int *cmap);
/** @brief Bandpass filtering for anti-aliasing the upsampled data.
Verasonics data is already demodulated. The upsampling procedure in
unwrapData results in an aliased spectrum. This kernel applies an
anti-aliasing bandpass filter whose band depends on the samples per cycle of
the VSXSampleMode.
*/
template <int spc>
__global__ void bandpassFilter(int nsamps, int nxmits, int nchans,
                               float2 *tdata, int tpitch);
/** @brief The final data is highly oversampled. Now that an anti-aliasing
 * bandpass filter has been applied, we can safely downsample the data without
 * loss of information.
 */
template <typename T, int spc>
__global__ void downsampleIQ(int nsamps_out, int nxmits, int nchans,
                             float2 *tdata, int tpitch, T *out, int opitch);
}  // namespace VSXDataFormatterKernels
}  // namespace rtbf

#endif
