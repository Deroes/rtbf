set(TARGETS_TO_BUILD L12_3v_bmode_synap)
# Loop over all targets
foreach(CURRENT_TARGET ${TARGETS_TO_BUILD})
	# Create MEX function
	matlab_add_mex(
		NAME ${CURRENT_TARGET}
		SRC ${CURRENT_TARGET}.cu
		LINK_TO gpuBF vsxBF_utils
	)
	# Need this line for proper linking with CUDA
	set_target_properties(${CURRENT_TARGET} PROPERTIES CUDA_RESOLVE_DEVICE_SYMBOLS ON)

	# Ask CMake to output the mex function into the current directory)
	RTBF_SET_MEX_OUTPUT_DIRECTORY(${CURRENT_TARGET} ${CMAKE_CURRENT_SOURCE_DIR})
endforeach()
