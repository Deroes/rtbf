% SetUpL12_3v_PlnWv_Bmode_tf.m:
% B-mode imaging using rtbf to perform plane wave compounding.
%
% This script utilizes the following gpuBF and vsxBF classes:
%   VSXDataFormatter
%   HilbertTransform (optional, if R.useAsIQ == 0)
%   FocusSynAp
%   ChannelSum
%   Bmode

clear all

% Hack to get rtbf directory, which is '../../../' of current script
tmp = split(which(mfilename), filesep);
rtbfdir = join(tmp(1:end-4), '/'); % ignore filename, go up 3 dirs
rtbfdir = rtbfdir{1};

% Add path to rtbf/vsxBF/functions
addpath(sprintf('%s/vsxBF/functions', rtbfdir));

%% Define acquisition parameters
% Hard parameters (cannot modify during acquisition)
P.na = 25;  % Number of plane wave angles
P.totalAngle = 10;  % Total angular span
P.configName = 'L12-3v_PlnWv_Bmode_tf';
P.sampleMode = 'NS200BW';  % [NS200BW, BS100BW, BS50BW] supported
% Modifiable parameters (using the GUI)
P.imageBufferSize = 40;  % Number of frames
P.rxStartMm = 1;  % Receive start depth
P.rxDepthMm = 30;  % Receive end depth
P.rxMaxDepthMm = 55;  % Maximum end depth
savedir = './data/';  % Directory where raw data is to be saved
P.acqFPS = 100;	 % frames per second of data being saved
P.acqPRF = 10e3;  % 10kHz PRF
P.TxVoltage = 0;  % To be populated later

%% Define reconstruction parameters
% R will contain all of the parameters used at the time of acquisition.
R.fovWidthMm = 20;  % Width of the field of view
R.outspwl = 3;  % Output pixel grid density (samples per wavelength)
R.fnum = 2;  % F-number for transmit and receive apodization
R.brange = [-50 0];  % Displayed dynamic range w.r.t. maximum value
R.useAsIQ = 0;  % 0: RF data (NS200BW only), 1: direct IQ sampling
R.uffFilename = sprintf('%s/annBF/bmode_network.uff', rtbfdir);

%% Populate the rest of P and R (do not modify!!!)
R.initialized = false;
% Use the right half of the aperture (aperture #65) for TX/RX
apers = 65 * ones(P.na,1);
% Set angles
if P.na == 1
	P.angles = 0;	P.dtheta = 0;
else
	P.angles = linspace(-P.totalAngle/2,P.totalAngle/2,P.na) * pi/180;
	P.dtheta = abs(diff(P.angles(1:2)));
end
P.np = 1; % No pulse compounding (e.g., pulse-inversion harmonic imaging)
if strcmp(P.sampleMode, 'NS200BW')
	P.spw = 4;
elseif strcmp(P.sampleMode, 'BS100BW')
	P.spw = 2;
elseif strcmp(P.sampleMode, 'BS50BW')
	P.spw = 1;
end
if R.useAsIQ == 0 && ~strcmp(P.sampleMode, 'NS200BW')
	error('To treat data as RF, P.sampleMode must be ''NS200BW''.')
end

% Flags for the external processing function
updatedRange = false;
quickUpdate = false; % Flag for small updates to mex function

%% Define system parameters.
Resource.Parameters.numTransmit = 128;  % number of transmit channels.
Resource.Parameters.numRcvChannels = 128;  % number of receive channels.
Resource.Parameters.simulateMode = 0;

%% Specify Trans structure array.
Trans.name = 'L12-3v';
Trans.units = 'wavelengths';  % required in Gen3 to prevent default to mm units
Trans.frequency = 7.813;  % center frequency for harmonic imaging receive
Trans = computeTrans(Trans);  % L12-3v transducer is 'known' transducer so we can use computeTrans.
Trans.maxHighVoltage = 30;

%% Define lengths in wavelengths now that we have Trans.frequency
mmToWvln = @(mm, fcMHz) mm*1e-3 * fcMHz*1e6 / 1540;
wvlnToMm = @(wvln, fcMHz) wvln*1540 / (fcMHz*1e6) * 1e3;
P.rxStart = mmToWvln(P.rxStartMm, Trans.frequency);
P.rxDepth = mmToWvln(P.rxDepthMm, Trans.frequency);
R.fovWidth = mmToWvln(R.fovWidthMm, Trans.frequency);
P.rxMaxDepth = mmToWvln(P.rxMaxDepthMm, Trans.frequency);

%% Specify Media object. 'pt1.m' script defines array of point targets.
pt1;
Media.function = 'movePoints';

%% Specify Resources.
% Compute the rows per frame (must be a multiple of 128)
rpf = (P.rxMaxDepth+20) * 2 * P.spw * P.na;
rpf = ceil(rpf / 128) * 128;
% Live imaging buffer
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = rpf;
Resource.RcvBuffer(1).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(1).numFrames = 2; % Only 2 frames needed for live acq
% Save data buffer
Resource.RcvBuffer(2).datatype = 'int16';
Resource.RcvBuffer(2).rowsPerFrame = rpf;
Resource.RcvBuffer(2).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(2).numFrames = P.imageBufferSize;

%% Specify Transmit waveform structure.
TW(1).type = 'parametric';
TW(1).Parameters = [Trans.frequency, 1, 2, 1];   % A, B, C, D

%% Specify TX structure array.
TX = repmat(struct('waveform', 1, ...
				   'Origin', [0, 0, 0], ...
				   'aperture', 1, ...
				   'Apod', ones(1,Resource.Parameters.numTransmit), ...
				   'focus', 0, ...
				   'Steer', [0, 0], ...
				   'Delay', zeros(1,Resource.Parameters.numTransmit)), 1, P.na);
% - Set event specific TX attributes.
for n = 1:P.na
	TX(n).aperture = apers(n);
	TX(n).Steer(1) = P.angles(n);
	TX(n).Delay = computeTXDelays(TX(n));
end

%% Specify TPC structures.
TPC(1).name = 'Imaging';
TPC(1).maxHighVoltage = 30;

%% Specify TGC Waveform structure.
TGC.CntrlPts = [442,599,728,795,914,981,1023,1023];
TGC.rangeMax = P.rxDepth;
TGC.Waveform = computeTGCWaveform(TGC);

%% Specify Receive structure arrays
%   endDepth - add additional acquisition depth to account for some channels
%              having longer path lengths.
%   InputFilter - The same coefficients are used for all channels. The
%              coefficients below give a broad bandwidth bandpass filter.
maxAcqLength = sqrt(P.rxDepth^2 + (Trans.numelements*Trans.spacing)^2) - P.rxStart;
wlsPer128 = 128/(P.spw*2); % wavelengths in 128 samples for 4 samplesPerWave
Receive = repmat(struct('Apod', ones(1,128), ...
						'aperture',1, ...
						'startDepth', P.rxStart, ...
						'endDepth', P.rxStart + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
						'TGC', 1, ...
						'bufnum', 1, ...
						'framenum', 1, ...
						'acqNum', 1, ...
						'sampleMode', P.sampleMode, ...
						'mode', 0, ...
						'callMediaFunc', 0),1, ...
						P.na*Resource.RcvBuffer(1).numFrames + ...
						P.na*Resource.RcvBuffer(2).numFrames);
% - Set event specific Receive attributes for each frame.
% Real-time imaging loop
for i = 1:Resource.RcvBuffer(1).numFrames
	offset = (i-1) * P.na;
	Receive(offset+1).callMediaFunc = 1;
	for n = 1:P.na
		Receive(offset+n).framenum = i;
		Receive(offset+n).aperture = apers(n);
		Receive(offset+n).acqNum = n;
	end
end

% Data saving loop
for i = 1:Resource.RcvBuffer(2).numFrames
	offset = (i-1) * P.na + P.na*Resource.RcvBuffer(1).numFrames;
	Receive(offset+1).callMediaFunc = 1;
	for n = 1:P.na
		Receive(offset+n).bufnum = 2;
		Receive(offset+n).framenum = i;
		Receive(offset+n).aperture = apers(n);
		Receive(offset+n).acqNum = n;
	end
end


%% Specify Process structure array.
% External function definition
import vsv.seq.function.ExFunctionDef;
EF(1).Function = ExFunctionDef('beamformRawData', @beamformRawData);
EF(2).Function = ExFunctionDef('saveBuffers', @saveBuffers);

Process(1).classname = 'External';
Process(1).method = 'beamformRawData';
Process(1).Parameters = {'srcbuffer','receive', ... % buffer to process
						 'srcbufnum',1, ... % live buffer
						 'srcframenum',-1, ... % most recent frame
						 'dstbuffer','none'}; % no output buffer

Process(2).classname = 'External';
Process(2).method = 'saveBuffers';
Process(2).Parameters = {'srcbuffer','receive', ... % buffer to process
						 'srcbufnum',2, ... % save buffer
						 'srcframenum',0, ... % all frames
						 'dstbuffer','none'}; % no output buffer
					 
%% Specify SeqControl structure arrays.
% Compute the time between the last acquisition and the next frame in usec
fullFrameTime = 1e6/P.acqFPS - (1e6/P.acqPRF*(P.na-1));
fprintf('Acquisition frame rate: %d fps.\n', P.acqFPS);
fprintf('Pulse reptition frequency: %.1fkHz.\n', P.acqPRF*1e-3);
if fullFrameTime < 10
	error('Timing for frame is too tight. Try decreasing the acquisition frame rate.')
end

% Timing sequence controls
SeqControl(1).command = 'timeToNextAcq';
SeqControl(1).argument = 1e6 / P.acqPRF;  % time between pulses
SeqControl(2).command = 'timeToNextAcq';
SeqControl(2).argument = fullFrameTime; % time between last pulse and next frame
% Sequence control to return to MATLAB
SeqControl(3).command = 'returnToMatlab';
% Sequence controls to jump
SeqControl(4).command = 'jump';
SeqControl(4).argument = 1;
% nsc is the count of SeqControl objects
nsc = length(SeqControl)+1;

%% Specify Event structure arrays.
n = 1;
for i = 1:Resource.RcvBuffer(1).numFrames
	frameStartIdx = (i-1) * P.na;
	% Acquire harmonic pulses
	for j = 1:P.na
		Event(n).info = 'Pulse Inversion TX';
		Event(n).tx = j;
		Event(n).rcv = j + frameStartIdx;
		Event(n).recon = 0;
		Event(n).process = 0;
		Event(n).seqControl = 1; % Acquire at the pulse repetition frequency
		n = n+1;
	end
	% Transfer frame
	Event(n-1).seqControl = [2, nsc]; % Acquire at the acquisition frame rate
	SeqControl(nsc).command = 'transferToHost'; % transfer frame to host buffer
	nsc = nsc+1;
	% Beamform first acquisition of frame
	Event(n).info = 'Beamform';
	Event(n).tx = 0;         % no transmit
	Event(n).rcv = 0;        % no rcv
	Event(n).recon = 0;      % reconstruction
	Event(n).process = 1;    % process
	Event(n).seqControl = 3;
	n = n+1;
end
Event(n).info = 'Jump back to beginning';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0;
Event(n).seqControl = 4;
n = n+1;

% Save data
saveEvent = n;
for i = 1:Resource.RcvBuffer(2).numFrames
	frameStartIdx = (i-1) * P.na + ...
		P.na*Resource.RcvBuffer(1).numFrames;
	% Acquire harmonic pulses
	for j = 1:P.na
		Event(n).info = 'Pulse Inversion TX';
		Event(n).tx = j;
		Event(n).rcv = j + frameStartIdx;
		Event(n).recon = 0;
		Event(n).process = 0;
		Event(n).seqControl = 1; % Acquire at the pulse repetition frequency
		n = n+1;
	end
	% Each image acquisition in the frame is at the acquisition frame rate
	Event(n-1).seqControl = [2, nsc];
	SeqControl(nsc).command = 'transferToHost'; % transfer frame to host buffer
	nsc = nsc+1;
end
Event(n).info = 'Wait for data';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0;
Event(n).seqControl = [2,nsc];
n = n+1;
SeqControl(nsc).command = 'waitForTransferComplete'; % Wait until the transfer is complete
SeqControl(nsc).argument = nsc-1; % Wait until previous transfer is completed.
nsc = nsc+1;

Event(n).info = 'Save raw data';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 2;
Event(n).seqControl = 0;
n = n+1;
Event(n).info = 'Jump back to first event';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0;
Event(n).seqControl = 4;
n = n+1;

%% User specified UI Control Elements
nui = 1;
% - Receive start depth change
UI(nui).Control = {'UserB8','Style','VsSlider','Label','Start Depth',...
	'SliderMinMaxVal',[0,40,P.rxStartMm],'SliderStep',[5 10]/40,'ValueFormat','%3.0f'};
UI(nui).Callback = text2cell('%-RxStartChangeCallback');
nui = nui+1;
% - Receive end depth change
UI(nui).Control = {'UserB7','Style','VsSlider','Label','End Depth',...
	'SliderMinMaxVal',[0,40,P.rxDepthMm],'SliderStep',[5 10]/40,'ValueFormat','%3.0f'};
UI(nui).Callback = text2cell('%-RxDepthChangeCallback');
nui = nui+1;
% - F-Number threshold change
UI(nui).Control = {'UserB6','Style','VsSlider','Label','f/#',...
	'SliderMinMaxVal',[.1,5,R.fnum],'SliderStep',[.1 .5]/4.9,'ValueFormat','%3.2f'};
UI(nui).Callback = text2cell('%-FNumberChangeCallback');
nui = nui+1;
% - Button to save data
UI(nui).Control = {'UserB5','Style','VsPushButton','Label','Save'}; % Hijack C5 UI
UI(nui).Callback = text2cell('%-SaveDataCallback');
nui = nui+1;

% - Receive field of view change
UI(nui).Control = {'UserC8','Style','VsSlider','Label','FOV Width',...
	'SliderMinMaxVal',[5,30,R.fovWidthMm],'SliderStep',[5 10]/25,'ValueFormat','%3.0f'};
UI(nui).Callback = text2cell('%-FovWidthChangeCallback');
nui = nui+1;
% - Output samples per wavelength change
UI(nui).Control = {'UserC7','Style','VsSlider','Label','Output SpWl',...
	'SliderMinMaxVal',[0.5,6,R.outspwl],'SliderStep',[.5 1]/5.5,'ValueFormat','%.2f'};
UI(nui).Callback = text2cell('%-OutSpwlChangeCallback');
nui = nui+1;

% Put B-mode, SLSC dynamic range controls in C5 slot.
cnames = {'Lo','Hi'};
rnames = {'B-mode'};
UI(nui).Statement = strjoin(cat(2,text2cell('%-DynamicRangeTable'), ...
	'drt.CellEditCallback = ''', ...
	strrep(strjoin(text2cell('%-DRTCellEditCallback')),'''',''''''),''';'));
nui = nui+1;	

%% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 1;

%% Save all the structures to a .mat file.
curdir = pwd;
sname = [pwd '/MatFiles/' P.configName];
disp(['filename = ''' sname '''; VSX'])
save(sname);
filename = sname;
VSX;

return

%% **** Callback routines to be converted by text2cell function. ****
%-RxStartChangeCallback - Range change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','P.rxStartMm'));
    return
end
P = evalin('base','P');
P.rxStartMm = UIValue;
% Make sure that P.rxStartMm is at least 5mm less than P.rxDepthMm
if P.rxStartMm > P.rxDepthMm-5
	P.rxStartMm = P.rxDepthMm-5;
end
% Make sure that GUI reflects the correct P.rxStartMm
if strcmp(Cntrl,'slider')
	set(h,'String',num2str(P.rxStartMm));
	set(hObject,'Value',P.rxStartMm);
else
	set(hObject,'String',num2str(P.rxStartMm));
	set(h,'Value',P.rxStartMm);
end
% If P.rxStartM has changed
if P.rxStartMm ~= evalin('base','P.rxStartMm')
	Trans = evalin('base','Trans');
	P.rxStart = P.rxStartMm*1e-3 * Trans.frequency*1e6 / 1540;
	Receive = evalin('base', 'Receive');
	for i = 1:size(Receive,2)
		Receive(i).startDepth = P.rxStart;
	end
	Control = evalin('base','Control');
	Control.Command = 'update&Run';
	Control.Parameters = {'PData','Receive','Recon','DisplayWindow','ImageBuffer'};
	clear L12_3v_bmode_synap_tf
	evalin('base','R.initialized = false;');
	assignin('base','P',P);
	assignin('base','Receive',Receive);
	assignin('base','Control', Control);
end
return
%-RxStartChangeCallback

%-RxDepthChangeCallback - Range change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','P.rxDepthMm'));
    return
end
P = evalin('base','P');
P.rxDepthMm = UIValue;
% Make sure that P.rxDepthMm is at least 5mm greater than P.rxStartMm
if P.rxDepthMm < P.rxStartMm+5
	P.rxDepthMm = P.rxStartMm+5;
end
% Make sure that GUI reflects the correct P.rxStartMm
if strcmp(Cntrl,'slider')
	set(h,'String',num2str(P.rxDepthMm));
	set(hObject,'Value',P.rxDepthMm);
else
	set(hObject,'String',num2str(P.rxDepthMm));
	set(h,'Value',P.rxDepthMm);
end
if P.rxDepthMm ~= evalin('base','P.rxDepthMm')
	Trans = evalin('base','Trans');
	P.rxDepth = P.rxDepthMm*1e-3 * Trans.frequency*1e6 / 1540;
	Receive = evalin('base', 'Receive');
	maxAcqLength = sqrt(P.rxDepth^2 + (Trans.numelements*Trans.spacing)^2) - P.rxStart;
	wlsPer128 = 128/(P.spw*2); % wavelengths in 128 samples for 4 samplesPerWave
	for i = 1:size(Receive,2)
		Receive(i).endDepth = P.rxStart + wlsPer128*ceil(maxAcqLength/wlsPer128);
	end
	TGC = evalin('base','TGC');
	TGC.rangeMax = P.rxDepth;
	TGC.Waveform = computeTGCWaveform(TGC);
	Control = evalin('base','Control');
	Control.Command = 'update&Run';
	Control.Parameters = {'PData','Receive','Recon','DisplayWindow','ImageBuffer'};
	clear L12_3v_bmode_synap_tf
	evalin('base','R.initialized = false;')
	assignin('base','P',P);
	assignin('base','Receive',Receive);
	assignin('base','TGC',TGC);
	assignin('base','Control',Control);
end
return
%-RxDepthChangeCallback

%-FovWidthChangeCallback - Field of view width change
R = evalin('base','R');
R.fovWidthMm = UIValue;
if R.fovWidthMm ~= evalin('base','R.fovWidthMm')
	Trans = evalin('base','Trans');
	R.fovWidth = R.fovWidthMm*1e-3 * Trans.frequency*1e6 / 1540;
	clear L12_3v_bmode_synap_tf
	evalin('base','R.initialized = false;')
	assignin('base','R',R);
end
return
%-FovWidthChangeCallback

%-OutSpwlChangeCallback - Pixel density change
R = evalin('base','R');
R.outspwl = UIValue;
if R.outspwl ~= evalin('base','R.outspwl')
	clear L12_3v_bmode_synap_tf
	evalin('base','R.initialized = false;')
	assignin('base','R',R);
end
return
%-OutSpwlChangeCallback

%-SaveDataCallback
% Just launch save script
saveEvent = evalin('base','saveEvent');
str = 'saveEvent';
Control = evalin('base','Control');
Control(1).Command = 'set&Run';
Control(1).Parameters = {'Parameters',1,'startEvent',saveEvent};
evalin('base',['Resource.Parameters.startEvent = ' str ';']);
assignin('base','Control', Control);
return
%-SaveDataCallback

%-DRTCellEditCallback
if any(isnan(drt.Data(:)))||any(drt.Data(2,:) >= drt.Data(1,:));
	drt.Data = [R.brange(2); R.brange(1)];
end;
R.brange(2) = drt.Data(1,1);
R.brange(1) = drt.Data(2,1);
assignin('base','R',R);
assignin('base','updatedRange',true);
%-DRTCellEditCallback

%-DynamicRangeTable
R = evalin('base','R');
drt = uitable(findobj('Tag','UI'));
drt.Data = [R.brange(2); R.brange(1)];
drt.ColumnWidth = {58,48};
drt.Units = 'normalized';
drt.Position = [0.67 0.43 0.32 0.13];
drt.ColumnName = {'B-mode'};
drt.ColumnEditable = true;
drt.RowName = {'Hi','Lo'};
drt.ColumnFormat = {'short','bank'};
drt.Position(4) = drt.Extent(4);
%-DynamicRangeTable

%-FNumberChangeCallback - Field of view width change
if UIValue ~= evalin('base','R.fnum')
	R = evalin('base','R');
	R.fnum = UIValue;
	assignin('base','R',R);
	clear L12_3v_bmode_synap_tf
	evalin('base','R.initialized = false;')
end
return
%-FNumberChangeCallback

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% External Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function saveBuffers(full_buf)

global b_buf ax lat frameNum fps
if isempty(fps)
	clear fps
end

savedir = evalin('base','savedir');
if ~exist(savedir,'dir'); mkdir(savedir); end
cur = [savedir datestr(now,'yyyymmdd_HHMMSS') '_rawbuf.mat'];

% Get structs
Resource = evalin('base', 'Resource');
Trans	 = evalin('base', 'Trans');
TW		 = evalin('base', 'TW');
TX		 = evalin('base', 'TX');
Receive  = evalin('base', 'Receive');
% Also get variables
P = evalin('base','P');
R = evalin('base','R');
P.TxVoltage = get(findobj(0,'Tag','hv1Value'),'String');

disp(['Saving ' cur '...'])
tic
fid = fopen([cur(1:end-4) '.bin'], 'wb');
full_buf_count = fwrite(fid,full_buf,'int16');
full_buf_size = size(full_buf);
fclose(fid); clear fid;
clear full_buf
save(cur, '-v6', '-regexp','^(?!(full_buf)$).')
fprintf('Took %.3f seconds to save data.\n', toc)
end

function beamformRawData(rcvbuf)

persistent h_bimg h_ax h_fps nsamps nxmits
persistent t1 t2
persistent b_max bufSize
global b_buf ax lat fps frameNum

% On first invocation, initialize
if ~evalin('base','R.initialized')
	% Get user parameters from base workspace
	R = evalin('base','R');
	P = evalin('base','P');	
	P.nxmits = P.na;
	% Grab relevant data structures from base workspace
	Resource = evalin('base','Resource');
	Receive	 = evalin('base','Receive');
	TX		 = evalin('base','TX');
	Trans	 = evalin('base','Trans');
	TW		 = evalin('base','TW');
	% Call the standalone processing function
	[lat, ax] = process_L12_3v_bmode_synap_tf(P, R, Resource, Receive, TX, Trans, TW);
	bimg = process_L12_3v_bmode_synap_tf(rcvbuf);
    bimg = 10*log10(bimg);  % Network outputs magnitude squared
	bimg(length(ax)+1:end,:) = []; % Remove extra rows at the end
	% Initialize cine-loop normalization vector, timing
	bufSize = 30; % Real-time cine-loop buffer size
	b_buf = zeros(size(bimg,1),size(bimg,2),bufSize,'single');
	b_max = max(bimg(:))*ones(bufSize,1,'single');
	% Subtract off max value
	bimg = bimg - max(bimg(:));		   
	% Plot for the first time
	figure(1001); clf
	aspectRatio = (lat(end)-lat(1)) / (ax(end)-ax(1));
	maxHeight = 800; maxWidth = 600;	
	if aspectRatio > maxWidth/maxHeight, width = maxWidth; height = maxWidth/aspectRatio;
	else, height = maxHeight; width = maxHeight*aspectRatio;
	end
	set(gcf,'Position',[100 350 width height]);
	colormap gray
	h_ax = gca;
	h_bimg = imagesc(lat, ax, bimg, R.brange); axis image
	set(h_ax,'Position',[0.05 0.05 0.9 0.9]);
	xlabel('Azimuth (mm)')
	ylabel('Depth (mm)')
	title('B-mode')
	colorbar
	h_fps = annotation('textbox',[0.45 0.02 0.1 0.05], 'String',sprintf('%2.0f fps',fps), ...
		'FontWeight', 'bold', 'FontSize', 14, 'Color','k','EdgeColor','none',...
		'HorizontalAlignment','center');
	frameNum = 1;
	t1 = tic-5e7;
	fps = 1;

	% Mark as initialized
	evalin('base','R.initialized = 1;');
else
	% Execute GPU BF
	bimg = process_L12_3v_bmode_synap_tf(rcvbuf);
    bimg = 10*log10(bimg);  % Network outputs magnitude squared
	bimg(length(ax)+1:end,:) = []; % Remove extra rows at the end
	b_max(frameNum) = max(bimg(:));
	% Use a history of maximum values to normalize
	% Use fps/2 frames (i.e. half a second)
	maxHistory = mod(frameNum+(-round(fps/2):0)-1, bufSize) + 1;
	bimg = bimg - mean(b_max(maxHistory));
	% Store in buffer
	b_buf(:,:,frameNum) = bimg;
	% Update image
	set(h_bimg, 'CData', bimg);
	if evalin('base', 'updatedRange')
		set(h_ax, 'CLim', R.brange);
		assignin('base','updatedRange',false);
	end
	% Update counters for fps and buffer
	frameNum = mod(frameNum,bufSize)+1;
	% Compute fps
	if mod(frameNum, 10) == 0
		t2 = toc(t1);
		fps = 10/t2;
		t1 = tic;
		set(h_fps,'String',sprintf(' %.0f fps',fps));
	end
end
end