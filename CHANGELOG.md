# Changelog

## [Version 1.0.0] as of 2021-05-27

This is the first "complete" major release of `rtbf`, with extensive testing on both Windows and Ubuntu systems. In particular, the CMake compilation is revamped to provide flexibility and compatibility. Also, we finally have our first [INSTALL.md](INSTALL.md) file that gives detailed step-by-step instructions on setting up the library.

### Added

- [INSTALL.md](INSTALL.md): Installation instructions for Windows and Linux.
- CMake options, as opposed to inferring modules to build based on environment variables. These variables (`RTBF_BUILD_*`) are provided in the top-level [CMakeLists.txt](CMakeLists.txt). Hopefully, this will provide flexibility in the future.
- New git-friendly format for users to specify their compilation options to CMake directly (`-D` options on command line, or cache entries via GUI, or JSON entries for Visual Studio Code). See [INSTALL.md](INSTALL.md) for more details.
- New requirement/suggestion for the users to provide their own `CMAKE_CUDA_ARCHITECTURES` option. Rather than wasting time and space generating code for every single target GPU architecture, users should now specify their exact architecture (61, 70, 86, etc.) to generate optimized code. See [INSTALL.md](INSTALL.md) for more details.
- New standalone processing code for `vsxBF` SetUp scripts. In prior versions, separate scripts were necessary for real-time and offline processing. Now, the same scripts can be used for both, simplifying processing and improving reusability. See [vsxBF/L12-3v/bmode/process_L12_3v_bmode_synap.m](vsxBF/L12-3v/bmode/process_L12_3v_bmode_synap.m) for an example.
- To use TensorRT on Windows, a path to the Protobuf library must also be provided as the configuration option `LIBPROTOBUF_DIR`.

### Removed

- `CMakeLists.txt.in`: The GoogleTest unit testing framework code was substantially simplified, so that this file was no longer necessary.

### Changed

- Enforced CMake version >= 3.19. We found that earlier versions (<= 3.12) led to strange `-pthread` errors and other unexplained bugs that are fixed with 3.19. Earlier versions of CMake may work, but they have not been tested.
- The OBJECT library approach is replaced by STATIC libraries for `gpuBF`, `vsxBF_utils`, and `annBF_utils`. The former approach was not playing nicely with Windows and Visual Studio (required fiddling with options via manual point-and-click). The static library approach also substantially simplifies and cleans up dependencies in the downstream vsxBF CMakeLists.txt files.
- The TensorRT location should now be specified as an ordinary CMake variable `${TENSORRT_DIR}`. Previously, the TensorRT root directory was specified via an environment variable `$ENV{TENSORRT_DIR}` (a clumsy hack in retrospect). Now that CMake configuration options are being used extensively, it makes more sense to consolidate all variables in one location.

## [Unreleased] as of 2021-03-11

### Important instructions for updates from 2021-03-09

The `initialize()` function arguments for `FocusSynAp` have changed to include a specifier for the new `enum class` `InterpMode` as the third-to-last argument. The new signature is

```c++
void initialize(DataArray<T_in> *input, int nOutputRows, int nOutputCols,
                float outputSampsPerWL, float *h_delTx, float *h_delRx,
                float *h_apoTx = nullptr, float *h_apoRx = nullptr,
                float samplesPerCycle = 0.f,
                SynthesisMode synthMode = SynthesisMode::SynthTx,
                InterpMode intrpMode = InterpMode::Cubic,
                cudaStream_t cudaStream = 0, int verbosity = 1);
```

For example, any code that previously initialized a `FocusSynAp` object `F` as

```c++
F.initialize(&DF, nrows, ncols, outspwl, delTx, delRx, apoTx, apoRx, spcy,
             synMode, stream, verbosity);
```

should be updated to

```c++
F.initialize(&DF, nrows, ncols, outspwl, delTx, delRx, apoTx, apoRx, spcy,
             synMode, intMode, stream, verbosity);  // Added intMode!
```

Coming soon, these updates will receive a version number, rather than being named [Unreleased].

## [Unreleased] as of 2021-03-09

### Added

- This [changelog](CHANGELOG.md) to keep users aware of changes and upgrades to `rtbf`.
- 1D interpolation routines as device template [code](gpuBF/interp1d.cuh).
- New `enum class InterpMode` to select interpolation mode.
  - `InterpMode::Linear` Linear interpolation
  - `InterpMode::Cubic` Cubic Hermite spline interpolation
  - `InterpMode::Lanczos3` Lanczos 3-lobe interpolation
- [vsxBF_utils](vsxBF/utils/vsxBF_utils.cuh) was updated to allow user specification of interpolation mode.

### Modified

- [VSXDataFormatter](vsxBF/utils/VSXDataFormatter.cuh) class received a substantial upgrade.
  - Prior implementation used linear interpolation of Q components of IQ, which may have led to spectral artifacts.
  - New code applies a proper sinc-filtered resampling of the demodulated data for `NS200BW`, `BS100BW`, and `BS50BW` options of `VSXSampleMode`. (The `HILBERT` option is preserved to obtain modulated output.)
    - The resampling is computed using a 1D FFT, bandpass filtering, followed by a 1D IFFT.
    - The output is automatically decimated to eliminate redundant samples.
  - Template kernels are added to avoid unnecessary code duplication.
  - The `HilbertTransform` nested object is replaced by effectively the same kernels.
- [FocusSynAp](gpuBF/FocusSynAp.cuh) class kernels were rewritten.
  - Now utilizes a single template kernel whose branches are instantiated and optimized at compile-time.
    - Branches based on `InterpMode`, baseband vs. RF, and no apodization vs. apodization.
  - More scalable approach than having 12 separate kernels for every possible combination of branching.

### Removed

- Removed broken `L12_3v_mlbf` examples. These will be restored once they have been fixed.
