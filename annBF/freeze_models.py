# File:       annBF/freeze_models.py
# Author:     Dongwoon Hyun (dongwoon.hyun@stanford.edu)
# Created on: 2019-07-18

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import tensorflow as tf
import uff
import subprocess
import os


def save_model_tf_keras(model, uff_filename):
    # First freeze the graph and remove training nodes
    output_names = model.output.op.name
    sess = tf.keras.backend.get_session()
    frozen_graph = tf.graph_util.convert_variables_to_constants(
        sess, sess.graph.as_graph_def(), [output_names]
    )
    frozen_graph = tf.graph_util.remove_training_nodes(frozen_graph)
    # Save the model
    pb_filename = "tmp.pb"
    with open(pb_filename, "wb") as ofile:
        ofile.write(frozen_graph.SerializeToString())
    convert_pb_to_uff(pb_filename, output_names, uff_filename)
    os.remove(pb_filename)  # Clean up .pb file


def save_model_tf(model_ckpt, output_names, uff_filename):
    # Load graph
    tf.reset_default_graph()
    with tf.Session() as sess:
        saver = tf.train.import_meta_graph("%s.meta" % model_ckpt, clear_devices=True)
        saver.restore(sess, model_ckpt)
        frozen_graph = tf.graph_util.convert_variables_to_constants(
            sess, tf.get_default_graph().as_graph_def(), output_names
        )
        pb_filename = "tmp.pb"
        with tf.gfile.GFile(pb_filename, "wb") as f:
            f.write(frozen_graph.SerializeToString())
    convert_pb_to_uff(pb_filename, output_names, uff_filename)
    os.remove(pb_filename)  # Clean up .pb file


def convert_pb_to_uff(pb_filename, output_names, uff_filename):
    # Convert to UFF using convert_to_uff utility
    subprocess.run(
        [
            "python",
            "%s/bin/convert_to_uff.py" % uff.__path__[0],
            pb_filename,
            "--output_node",
            output_names,
            "--output",
            uff_filename,
        ]
    )
