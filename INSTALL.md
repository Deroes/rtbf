# Installation Instructions for `rtbf`

Recognizing that `rtbf` users will have varying levels of experience with C++ and CMake, here are detailed step-by-step instructions on how to properly set up the CMake environment.

## Dependencies

Required

- CMake (version >= 3.19)
- GCC for Linux (tested only on GCC >= 7) or MSVC for Windows (tested only on >= VS2019)
- CUDA (tested on version >= 10.x)

Optional to interface with Verasonics and TensorRT

- MATLAB (requires version >= R2019b)
- TensorRT (tested on version >= 7.x.x.x)

## Download

To install the code, git clone the repository into your desired directory:

```bash
git clone https://gitlab.com/dongwoon.hyun/rtbf.git
```

## Configuration Options

Configuration options are used to generate the Makefiles or Visual Studio solutions that are then compiled into libraries and executables. We first list several important ones here, with specific instructions on how to use them given further below.

- `rtbf` itself has several configuration options, listed in the top-level [`CMakeLists.txt`](CMakeLists.txt). These options control whether to compile `vsxBF` (`RTBF_BUILD_VSXBF`) and `annBF` (`RTBF_BUILD_ANNBF`), among other things.

- One CUDA configuration option that users will likely want to specify is `CMAKE_CUDA_ARCHITECTURES=XX`, where `XX` is the compute capability of your GPU. This tells the `nvcc` compiler to generate optimized code specifically for the given GPU architecture. I strongly recommend including this in the call to `cmake`. The code can *technically* compile without this flag, but any architecture-specific compilation will be offloaded to the just-in-time (JIT) compiler, which can lead to a long delay the first time code is executed, as well as potentially suboptimal code.

- If CMake has a hard time finding the right version of CUDA or MATLAB, you may need to specify their respective root directories as options as well (e.g., `Matlab_ROOT_DIR=/usr/local/MATLAB/R2019b`).

## Instructions for **Linux** (Command Line)

These instructions are for an out-of-place build in Linux.

1. First, create a subdirectory inside of `rtbf` called `build` and move into it:

    ```bash
    mkdir <rtbf_directory>/build
    cd <rtbf_directory>/build
    ```

2. Next, execute the **configuration** step from inside the `build` folder:

    ```bash
    cmake .. <OPTIONS>
    ```

    Here, `<OPTIONS>` are any configuration options that you would like to enable. See the **Configuration Options** sub-heading above for more information. For example, to compile `rtbf` for Verasonics execution with a NVIDIA GeForce GTX 1080 Ti GPU (compute capability 6.1), one would prefix each option with "-D" as:

    ```bash
    cmake .. -DCMAKE_CUDA_ARCHITECTURES=61 -DRTBF_BUILD_VSXBF=ON
    ```

    **NOTE:** If you encounter errors that CMake could not find MATLAB even though you have version >=R2019b installed, you can explicitly pass the Matlab root directory as a flag, e.g.:

    ```bash
    cmake .. -DMatlab_ROOT_DIR="/usr/local/MATLAB/R2019b" <OTHER_OPTIONS>
    ```

3. Finally, execute the **build** step (still inside the `build` folder):

    ```bash
    make -j8
    ```

    The `-j8` option is used for a multi-threaded build (8 threads). This step will generate all of the libraries (`libgpuBF.a`, etc.), and if enabled, any MATLAB MEX functions included `vsxBF`.

## Instructions for **Windows** (CMake-GUI)

These instructions are for an out-of-place build using the Windows CMake-GUI.

1. In "Where is the source code:", enter the directory to `rtbf`, which we'll call `<rtbf_directory>` from here on.

2. In "Where to build the binaries:", enter `<rtbf_directory>/build`.

3. Add a cache entry (click "+ Add Entry") to add options. See the **Configuration Options** sub-heading above for more information. For example, to compile `rtbf` for Verasonics execution with a NVIDIA GeForce GTX 1080 Ti GPU (compute capability 6.1), you would add two entries:

    1. CUDA architecture
        - Name: CMAKE_CUDA_ARCHITECTURES
        - Type: STRING
        - Value: 61
    2. Verasonics MEX compilation
        - Name: RTBF_BUILD_VSXBF
        - Type: BOOL
        - Value: ON (check mark)

4. Click "Configure". This will prompt you to create the `build` directory if it does not exist. This should then bring up a prompt to specify the generator for this project.
    - Select "Visual Studio 16 2019" (or another appropriate generator).
    - Leave the "Optional platform" and "Optional toolset" fields blank and choose "Use default native compilers", unless you are familiar with these options and need different functionality.
    - Press Finish.

5. Click "Generate".

6. Click "Open Project". This will open up Visual Studio, where you can build the project using the built-in functionality.

## Instructions for **Linux** and **Windows** (Visual Studio Code)

Finally, here are instructions for using Microsoft Visual Studio Code (NOTE: ***not*** Microsoft Visual Studio).

I use VS Code extensively as my primary development environment both on Linux and Windows, and highly recommend it. I provide these instructions simply because VS Code and its features have saved me countless hours in coding time, and I would like to pass those savings on to you. In particular, I like VS Code because it is lightweight, has good intellisense and syntax highlighting, supports remote development via SSH, and has many great extensions. Underrated features include multi-cursors and code autoformatting, which have eliminated many repetitive and trivial tasks (e.g., same edit to multiple lines, worrying about indentations and spacings) and have freed me up to focus on the actual research at hand. It also allows me to use the same seamless setup for both Windows and Linux.

Of course, this is just my personal opinion based on my own experience; you are free to use whatever IDE you please.

1. VS Code Setup

    I use the following extensions for CMake building in VS Code:

    - CMake Tools (Microsoft) to configure and build
    - C/C++ (Microsoft) for syntax highlighting in C++ files
    - vscode-cudacpp (kriegalex) for syntax highlighting in CUDA files
    - CMake (twxs) for syntax highlighting in CMake files

    I also like to install the following extensions for convenience:

    - Clang-Format (xaver) to automatically format my code according to Google's specification
    - Test Explorer UI (Holger Benl) to quickly run the unit tests
    - C++ TestMate (Mate Pek) to automatically detect the GoogleTests in my directory
    - Remote - SSH (Microsoft) for SSH-ing into both my Linux and Windows machines

2. Open the `rtbf` folder in VS Code

3. On the left sidebar, go to the CMake icon (triangle inside of rectangle). At the top, next to "CMAKE: PROJECT OUTLINE", there are several small icons. The sheet of paper with an arrow is the **configure** icon, and the down arrow into the box is the **build** icon.

    To configure the code, click the "sheet of paper with a right arrow" icon.

    To build the code, click the "down arrow into the box" icon.

    To specify any configuration options, you can edit the `.vscode/settings.json` file for the current workspace (create one if it does not exist). For example, to configure CMake to build `vsxBF` and `annBF` for a GPU architecture of 61, you would add the following snippet to the file:

    ```json
    {
        "cmake.configureArgs": [
            "-DRTBF_BUILD_VSXBF=ON",
            "-DRTBF_BUILD_ANNBF=OON",
            "-DCMAKE_CUDA_ARCHITECTURES=61"
        ]
    }
    ```
