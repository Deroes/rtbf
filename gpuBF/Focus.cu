/**
 @file gpuBF/Focus.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Focus.cuh"

namespace rtbf {
namespace kernels = FocusKernels;
using IM = InterpMode;

template <typename T_in, typename T_out>
Focus<T_in, T_out>::Focus() {
  this->resetDataProcessor();
  reset();
}
template <typename T_in, typename T_out>
Focus<T_in, T_out>::~Focus() {
  reset();
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::reset() {
  CCE(cudaSetDevice(this->devID));
  if (this->isInit) {
    this->printMsg("Clearing object.");
    CCE(cudaDestroyTextureObject(tex));
  }
  // Focus members
  memset(&texDesc, 0, sizeof(texDesc));
  memset(&resDesc, 0, sizeof(resDesc));
  raw = nullptr;
  spc = 0.f;
  del.reset();
  apo.reset();

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "Focus");
}

// Initializer
template <typename T_in, typename T_out>
void Focus<T_in, T_out>::initialize(DataArray<T_in> *input, int nOutputRows,
                                    int nOutputCols, float outputSampsPerWL,
                                    float *h_del, float *h_apo,
                                    float samplesPerCycle,
                                    cudaStream_t cudaStream, int verbosity) {
  // If previously initialized, reset
  reset();

  // Set DataProcessor members
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Parse inputs
  raw = input;
  int nchans = raw->getNc();
  outwlps = 1.f / outputSampsPerWL;

  // Initialize arrays for output, delays, apodization
  CCE(cudaSetDevice(this->devID));
  this->out.initialize(nOutputRows, nOutputCols, nchans, raw->getNf(),
                       this->devID, "Focus", this->verb);
  del.initialize(this->out.getDataDim(), this->devID, "Focus: Delay table",
                 this->verb);
  del.copyToGPU(h_del);
  if (h_apo != nullptr) {
    apo.initialize(this->out.getDataDim(), this->devID, "Focus: Apodization",
                   this->verb);
    apo.copyToGPU(h_apo);
  }

  // Initialize texture object for input
  initTextureObject(raw->getDataPtr());

  // Mark as initialized
  this->isInit = true;
}

// Initializer for DataProcessor input
template <typename T_in, typename T_out>
void Focus<T_in, T_out>::initialize(DataProcessor<T_in> *input, int nOutputRows,
                                    int nOutputCols, float outputSampsPerWL,
                                    float *h_del, float *h_apo,
                                    float samplesPerCycle,
                                    cudaStream_t cudaStream, int verbosity) {
  // Pass through to other initializer
  initialize(input->getOutputDataArray(), nOutputRows, nOutputCols,
             outputSampsPerWL, h_del, h_apo, samplesPerCycle, cudaStream,
             verbosity);
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::initTextureObject(T_in *d_ptr) {
  // Set up textures to use the built-in bilinear interpolation hardware
  // Use texture objects (CC >= 3.0)
  // Texture description
  texDesc.addressMode[0] = cudaAddressModeBorder;
  texDesc.addressMode[1] = cudaAddressModeBorder;
  texDesc.filterMode = cudaFilterModeLinear;
  texDesc.normalizedCoords = 0;
  texDesc.readMode = cudaReadModeElementType;
  // Only if input is 16-bit integer and output is float2
  if (typeid(T_in) == typeid(short2) && typeid(T_out) == typeid(float2))
    texDesc.readMode = cudaReadModeNormalizedFloat;
  // Resource description
  DataDim d = raw->getDataDim();
  resDesc.resType = cudaResourceTypePitch2D;
  resDesc.res.pitch2D.width = d.x;
  resDesc.res.pitch2D.height = d.y * d.c;
  resDesc.res.pitch2D.desc = cudaCreateChannelDesc<T_in>();
  resDesc.res.pitch2D.devPtr = d_ptr;
  resDesc.res.pitch2D.pitchInBytes = raw->getPitchInBytes();
  // Create texture object
  CCE(cudaSetDevice(this->devID));
  CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(raw->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  CCE(cudaSetDevice(this->devID));
  CCE(cudaDestroyTextureObject(tex));
  raw = input;
  initTextureObject(raw->getDataPtr());
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::addGlobalDelay(float globalDelay) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the delay table dimensions
  DataDim dims = del.getDataDim();
  CCE(cudaSetDevice(this->devID));
  dim3 B(256, 1, 1);
  dim3 G((dims.x - 1) / B.x + 1, (dims.y - 1) / B.y + 1,
         (dims.c - 1) / B.z + 1);
  kernels::addGlobalDelay<<<G, B, 0, this->stream>>>(dims, del.getDataPtr(),
                                                     globalDelay);
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::focus() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  CCE(cudaSetDevice(this->devID));
  // Determine which CUDA kernel to call
  bool isRF = spc == 0.f;
  bool isApo = apo.getDataPtr() != nullptr;
  // Call the focus kernel. Explicitly specify the kernel template parameters
  // to achieve implicit template function instantiation. The struct focusArgs
  // and function focusHelper are used purely to make the following code more
  // readable.
  if (imode == IM::Linear) {
    if (isRF && isApo) focusHelper<IM::Linear, true, true>();
    if (isRF && !isApo) focusHelper<IM::Linear, true, false>();
    if (!isRF && isApo) focusHelper<IM::Linear, false, true>();
    if (!isRF && !isApo) focusHelper<IM::Linear, false, false>();
  } else if (imode == IM::Cubic) {
    if (isRF && isApo) focusHelper<IM::Cubic, true, true>();
    if (isRF && !isApo) focusHelper<IM::Cubic, true, false>();
    if (!isRF && isApo) focusHelper<IM::Cubic, false, true>();
    if (!isRF && !isApo) focusHelper<IM::Cubic, false, false>();
  } else if (imode == IM::Lanczos3) {
    if (isRF && isApo) focusHelper<IM::Lanczos3, true, true>();
    if (isRF && !isApo) focusHelper<IM::Lanczos3, true, false>();
    if (!isRF && isApo) focusHelper<IM::Lanczos3, false, true>();
    if (!isRF && !isApo) focusHelper<IM::Lanczos3, false, false>();
  }
}

template <typename T_in, typename T_out>
template <InterpMode imode, bool isRF, bool isApo>
void Focus<T_in, T_out>::focusHelper() {
  // Grab pointers and pitches
  DataDim odims = this->out.getDataDim();
  int delPitch = del.getNp();
  int outPitch = this->out.getNp();
  float *d_del = del.getDataPtr();
  float *d_apo = apo.getDataPtr();
  int nrows = odims.x;
  int ncols = odims.y;
  int nchans = odims.c;

  // Kernel launch parameters
  dim3 B(16, 4, 4);
  dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
         (odims.c - 1) / B.z + 1);
  float normFactor =
      (typeid(T_in) == typeid(short2) && typeid(T_out) == typeid(float2))
          ? 32767.f
          : 1.f;

  // Loop through frames
  this->out.resetToZeros();
  for (int f = 0; f < raw->getNf(); f++) {
    CCE(cudaSetDevice(this->devID));
    CCE(cudaDestroyTextureObject(tex));
    initTextureObject(raw->getFramePtr(f));
    T_out *d_out = this->out.getFramePtr(f);
    kernels::focus<T_out, imode, isRF, isApo><<<G, B, 0, this->stream>>>(
        tex, nrows, ncols, nchans, d_del, d_apo, delPitch, d_out, outPitch,
        outwlps, normFactor, 1.f / spc);
    getLastCudaError("Focusing kernel failed.\n");
  }
}

///////////////////////////////////////////////////////////////////////////
// Kernels
///////////////////////////////////////////////////////////////////////////
// Core kernel with template options for conditional execution. Templatizing
// the kernel allows the compiler to make branch-wise optimizations at
// compile-time. That is, the GPU will not actually go through the if/else
// branches for template parameters, and will instead execute the correct
// compiled version for each branch.
template <typename T_out, IM imode, bool isRF, bool isApo>
__global__ void kernels::focus(cudaTextureObject_t tex, int nrows, int ncols,
                               int nchans, float *del, float *apo, int delPitch,
                               T_out *out, int outPitch, float outwlps,
                               float normFactor, float cyclesPerSample) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // kept dimension
  // Boundary check
  if (row < nrows && col < ncols && chan < nchans) {
    // Advance output pointer to current thread
    out += row + outPitch * (col + ncols * chan);
    // No need to compute if the apodization is zero
    float a = isApo ? apo[row + delPitch * (col + ncols * chan)] : 1.f;
    if (a == 0.f) {
      saveData2(out, 0, make_float2(0.f));
      return;
    }
    // Otherwise, compute everything
    // Otherwise, get the delay (x) and the transmit/channel (y) to retrieve
    float x = del[row + delPitch * (col + ncols * chan)];
    float y = col + ncols * chan;
    // Interpolate
    float2 data;
    if (imode == IM::Linear) {
      data = interp1d::linear<float2>(tex, x, y);
    } else if (imode == IM::Cubic) {
      data = interp1d::cubic<float2>(tex, x, y);
    } else if (imode == IM::Lanczos3) {
      data = interp1d::lanczos3<float2>(tex, x, y);
    }
    if (isApo) data *= a;  // Apply apodization if provided
    if (!isRF) {     // If baseband, undo the initial phase due to demodulation
      float2 shift;  // Get the phase rotation complex phasor
      sincospif(2.0f * x * cyclesPerSample, &shift.y, &shift.x);
      data = complexMultiply(data, shift);  // Multiply
    }
    // // Demodulate signal
    // float2 demod;  // Get the demodulation complex phasor
    // sincospif(-4.0f * row * outwlps, &demod.y, &demod.x);
    // data = complexMultiply(data, demod);  // Apply demodulation
    data *= normFactor;  // Undo the CUDA texture normalization factor
    // Cast sum to correct datatype and store result in d_out
    saveData2(out, 0, data);
  }
}

// Kernel to add scalar constant to entire delay table
__global__ void kernels::addGlobalDelay(DataDim dims, float *del,
                                        float globalDelay) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;
  int col = blockIdx.y * blockDim.y + threadIdx.y;
  int page = blockIdx.z * blockDim.z + threadIdx.z;
  if (row < dims.x && col < dims.y && page < dims.c) {
    int idx = row + dims.p * (col + dims.y * page);
    del[idx] += globalDelay;
  }
}

template class Focus<short2, short2>;
template class Focus<short2, float2>;
template class Focus<float2, float2>;
}  // namespace rtbf
