/**
 @file gpuBF/ChannelCoherence.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2020-02-06

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CHANNELCOHERENCE_CUH_
#define CHANNELCOHERENCE_CUH_

#include "DataProcessor.cuh"

namespace rtbf {

/**	@brief Class to compute the coherence between channels.

This class computes the correlation coefficient as a function of channel
spacing. Here, the wide-sense stationarity assumption is applied across
channels, i.e., the coherence is assumed to depend only on channel spacing,
called the lag. Therefore the output is the coherence as a function of lag.
*/
template <typename T_in, typename T_out>
class ChannelCoherence : public DataProcessor<T_out> {
 private:
  DataArray<T_in> *in;  ///< Input DataArray
  int nlags;            ///< Number of output lags

 public:
  ChannelCoherence();
  virtual ~ChannelCoherence();

  /// @brief Initialize ChannelCoherence object with a DataArray input
  void initialize(DataArray<T_in> *input, int numLags,
                  cudaStream_t cudaStream = 0, int verbosity = 1);
  /// @brief Initialize ChannelCoherence object with a DataProcessor input
  void initialize(DataProcessor<T_in> *input, int numLags,
                  cudaStream_t cudaStream = 0, int verbosity = 1);
  /// @brief Reset object
  void reset();

  /// @brief Compute the channel coherence
  void computeCoherence(int axkern = 1, int latkern = 1);
  /// @brief Set a new input DataArray
  void setInputDataArray(DataArray<T_in> *input);
  /// @brief Set a new input DataArray from a DataProcessor
  void setInputDataProcessor(DataProcessor<T_in> *input) {
    setInputDataArray(input->getOutputDataArray());
  }
};

namespace ChannelCoherenceKernels {
/// @brief Complex multiplication, real-component only
__device__ float cmultR(float2 a, float2 b);
/// @brief Single-sample correlation coefficient
template <typename T_in, typename T_out>
__global__ void corr0D(T_in *dat, DataDim idims, T_out *ccs, DataDim odims);
/// @brief Correlation coefficient with axial kernel
template <typename T_in, typename T_out>
__global__ void corr1D(T_in *dat, DataDim idims, T_out *ccs, DataDim odims,
                       int halfkA);
/// @brief Correlation coefficient with 2D kernel
template <typename T_in, typename T_out>
__global__ void corr2D(T_in *dat, DataDim idims, T_out *ccs, DataDim odims,
                       int halfkA, int halfkL);
}  // namespace ChannelCoherenceKernels
}  // namespace rtbf

#endif /* CHANNELCOHERENCE_CUH_ */
