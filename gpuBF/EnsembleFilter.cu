/**
 @file gpuBF/EnsembleFilter.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-07-30

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "EnsembleFilter.cuh"

namespace rtbf {
namespace kernels = EnsembleFilterKernels;

template <typename T>
EnsembleFilter<T>::EnsembleFilter() {
  reset();
}
template <typename T>
EnsembleFilter<T>::~EnsembleFilter() {
  reset();
}
template <typename T>
void EnsembleFilter<T>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");
  CCE(cudaSetDevice(this->devID));

  // EnsembleFilter class members
  in = nullptr;
  filt.reset();

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "EnsembleFilter");
}
template <typename T>
void EnsembleFilter<T>::initialize(DataArray<T> *input, int nrows, int ncols,
                                   float *h_filterMatrix,
                                   cudaStream_t cudaStream, int verbosity) {
  // If previously initialized, reset
  reset();

  // Set DataProcessor members
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Parse inputs
  in = input;
  M = nrows;
  N = ncols;

  // Error checking
  if (in->getNf() != M) {
    this->printErr(
        "# of filter matrix rows does not match # of input data frames.\n",
        __FILE__, __LINE__);
  }
  if (N > M) {
    this->printErr("In place multiplication cannot be performed (M < N).\n",
                   __FILE__, __LINE__);
  }
  if (M > MAXENSEMBLELENGTH) {
    this->printErr("M is greater than MAXENSEMBLELENGTH.\n", __FILE__,
                   __LINE__);
  }

  // Copy filter coefficients from host to device
  CCE(cudaSetDevice(this->devID));
  filt.initialize(M * N, 1, 1, 1, this->devID, "Filter matrix");
  filt.copyToGPU(h_filterMatrix);

  // Mark as initialized
  this->isInit = true;
}

template <typename T>
void EnsembleFilter<T>::initialize(DataProcessor<T> *input, int nrows,
                                   int ncols, float *h_filterMatrix,
                                   cudaStream_t cudaStream, int verbosity) {
  // Pass data to initialize function
  initialize(input->getOutputDataArray(), nrows, ncols, h_filterMatrix,
             cudaStream, verbosity);
}

// Matrix multiplication with float input
template <typename T>
void EnsembleFilter<T>::filterEnsemble() {
  CCE(cudaSetDevice(this->devID));
  DataDim dim = in->getDataDim();

  // This function applies the filter to both the real and imaginary components
  int smemSize = sizeof(float) * M * N;
  dim3 B(256, 1, 1);
  dim3 G((dim.x - 1) / B.x + 1, (dim.y - 1) / B.y + 1, (dim.c - 1) / B.z + 1);
  kernels::filter<<<G, B, smemSize, this->stream>>>(in->getDataPtr(), dim, M, N,
                                                    filt.getDataPtr());
}

template <typename T>
__global__ void kernels::filter(T *dat, DataDim dim, int M, int N,
                                float *filt) {
  extern __shared__ float fc[];  // Use shared memory for filter coefficients
  // Information about the thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;
  int col = blockIdx.y * blockDim.y + threadIdx.y;
  int chan = blockIdx.z * blockDim.z + threadIdx.z;
  int tidx =
      threadIdx.x + blockDim.x * (threadIdx.y + blockDim.y * threadIdx.z);
  int didx = row + dim.p * (col + dim.y * chan);
  int stride = dim.p * dim.y * dim.c;

  // Load filter coefficients into shared memory
  if (tidx < M * N) fc[tidx] = filt[tidx];
  __syncthreads();

  // Limit computation to valid indices
  if (row < dim.x && col < dim.y && chan < dim.c) {
    // Preload all data frames of current pixel into registers
    float2 d[MAXENSEMBLELENGTH];
    for (int m = 0; m < M; m++) {
      // Load data as float2
      d[m] = loadFloat2(dat, didx + stride * m);
    }
    // Compute each output frame
    for (int n = 0; n < N; n++) {
      float2 sum = make_float2(0.f, 0.f);
      for (int m = 0; m < M; m++) {
        sum.x += d[m].x * fc[m + M * n];
        sum.y += d[m].x * fc[m + M * n];
      }
      saveData2(dat, didx + stride * n, sum);
    }
    // Set any extra frames (N+1 to M) to zero
    for (int n = N; n < M; n++) {
      saveData2(dat, didx + stride * n, make_float2(0.f, 0.f));
    }
  }
}

template class EnsembleFilter<float2>;
template class EnsembleFilter<short2>;

}  // namespace gpuBF
